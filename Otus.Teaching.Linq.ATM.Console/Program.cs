﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var login = "snow";
            var password = "111";

            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            System.Console.WriteLine("Вывод информации о заданном аккаунте по логину и паролю:");

            try
            {
                var user = atmManager.GetUserByLoginAndPassword(login, password);
                if (user != null)
                {
                    System.Console.WriteLine(user.ToString());
                    System.Console.WriteLine(System.Environment.NewLine);

                    System.Console.WriteLine("Вывод данных о всех счетах заданного пользователя:");

                    var accounts = atmManager.GetAccountsData(user);
                    foreach (var account in accounts)
                    {
                        System.Console.WriteLine(account.ToString());
                    }
                    System.Console.WriteLine(System.Environment.NewLine);

                    System.Console.WriteLine("Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту:");

                    var accountsWithHistory = atmManager.GetAccountsWithOperationHistory(user);
                    foreach (var data in accountsWithHistory)
                    {
                        System.Console.WriteLine(data.Key.ToString());
                        foreach (var history in data.Value)
                        {
                            System.Console.WriteLine(history.ToString());
                        }
                        System.Console.WriteLine(System.Environment.NewLine);
                    }
                }
                else
                {
                    System.Console.WriteLine("Пользователь с заданным логином и паролем не найден.\n");
                }

            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Найдено несколько совпадений пользователей по заданному логину и паролю.\n");
            }

            System.Console.WriteLine("Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта:");

            var allAccountsHistory = atmManager.GetAllHistoryAboutInputCashAccounts();
            foreach (var accountHistory in allAccountsHistory)
            {
                System.Console.WriteLine(accountHistory.User.FirstName + " " + accountHistory.User.MiddleName);
                foreach (var cashHistory in accountHistory.History)
                {
                    System.Console.WriteLine(cashHistory.ToString());
                }
                System.Console.WriteLine(System.Environment.NewLine);
            }

            System.Console.WriteLine("Вывод данных о всех пользователях у которых на счёте сумма больше N:");

            decimal sum = 100500m;
            var users = atmManager.GetUsersByAccountAmount(sum);
            foreach (var currentUser in users)
            {
                System.Console.WriteLine(currentUser.ToString());
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}