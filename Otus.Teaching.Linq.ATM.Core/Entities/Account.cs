﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime OpeningDate { get; set; }
        public decimal CashAll { get; set; }
        public int UserId { get; set; }

        public override string ToString()
        {
            string result = $"{this.Id}, {this.Id}, {this.OpeningDate:dd.MM.yyyy}, {this.CashAll}m, {this.UserId}";
            return result;
        }
    }
}