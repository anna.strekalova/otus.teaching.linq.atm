﻿using System.Collections.Generic;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class UserHistory
    {
        public User User { get; set; }
        public IEnumerable<OperationsHistory> History { get; set; }
    }
}
