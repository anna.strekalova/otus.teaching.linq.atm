﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    /// <summary>
    /// Класс предоставляющий логику работы с банковскими данными
    /// </summary>
    public class ATMManager
    {
        /// <summary>
        /// Счета пользователей
        /// </summary>
        public IEnumerable<Account> Accounts { get; private set; }

        /// <summary>
        /// Пользователи
        /// </summary>
        public IEnumerable<User> Users { get; private set; }

        /// <summary>
        /// История операций 
        /// </summary>
        public IEnumerable<OperationsHistory> History { get; private set; }


        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        /// <summary>
        /// Получить пользователя по логину и паролю
        /// </summary>
        /// <param name="login">
        /// Логин пользователя
        /// </param>
        /// <param name="password">
        /// Пароль пользователя
        /// </param>
        /// <returns>
        /// Пользователь соответствующий логину и паролю
        /// </returns>
        public User GetUserByLoginAndPassword(string login, string password)
        {
            var user = this.Users.SingleOrDefault(x => x.Login.Equals(login) && x.Password.Equals(password));
            return user;
        }

        /// <summary>
        /// Получить все счета пользователя
        /// </summary>
        /// <param name="user">
        /// Пользователь
        /// </param>
        /// <returns>
        /// Список счетов пользователя
        /// </returns>
        public List<Account> GetAccountsData(User user)
        {
            var accounts = this.Accounts.Where(x => x.UserId.Equals(user.Id)).ToList();
            return accounts;
        }

        /// <summary>
        /// Получить все счета пользователя, включая историю по каждому счёту
        /// </summary>
        /// <param name="user">
        /// Пользователь
        /// </param>
        /// <returns>
        /// Коллекция счета пользователя, включая историю по каждому счёту
        /// </returns>
        public Dictionary<Account, IEnumerable<OperationsHistory>> GetAccountsWithOperationHistory(User user)
        {
            var data = this.Accounts.Where(x => x.UserId == user.Id).GroupJoin(
                this.History,
                t => t.Id,
                p => p.AccountId,
                (acc, hist) => new
                {
                    Account = acc,
                    History = hist
                }).ToDictionary(n => n.Account, n => n.History);
            return data;
        }

        /// <summary>
        /// Получить все операции пополнения счёта с указанием владельца каждого счёта
        /// </summary>
        /// <returns>
        /// Коллекция всех операций пополнения счёта с указанием владельца каждого счёта
        /// </returns>
        public IEnumerable<UserHistory> GetAllHistoryAboutInputCashAccounts()
        {
            var data = this.Accounts.Join(this.Users,
                x => x.UserId,
                t => t.Id,
                (acc, user) => new UserHistory()
                {
                    User = user,
                    History = this.History.Where(p => p.OperationType == OperationType.InputCash && p.AccountId == acc.Id)
                }).Where(n => n.History.Count() > 0);

            return data;
        }

        /// <summary>
        /// Получить всех пользователей у которых на счёте сумма больше N
        /// </summary>
        /// <param name="N">
        /// Сумма
        /// </param>
        /// <returns>
        /// Список пользователей у которых на счёте сумма больше N
        /// </returns>
        public List<User> GetUsersByAccountAmount(decimal N)
        {
            var data = this.Users.Join(this.Accounts.Where(x => x.CashAll > N).GroupBy(t => t.UserId),
                p => p.Id,
                n => n.Key,
                (user, acc) => user).ToList();

            return data;
        }
    }
}